package ca.csf.mobile2.tp2.question.createQuestion

import androidx.databinding.BaseObservable
import androidx.databinding.Observable
import ca.csf.mobile2.tp2.question.Question
import org.parceler.Parcel
import org.parceler.ParcelConstructor
import org.parceler.Transient

@Parcel(Parcel.Serialization.BEAN)
class CreateQuestionActivityViewModel @ParcelConstructor constructor(
    val question: Question
) : BaseObservable() {

    @get:Transient
    var text: String
        get() = question.text
        set(value) {
            question.text = value
        }

    @get:Transient
    var choice1: String
        get() = question.choice1
        set(value) {
            question.choice1 = value
        }

    @get:Transient
    var choice2: String
        get() = question.choice2
        set(value) {
            question.choice2 = value
        }

    @get:Transient
    var isLoading: Boolean = false
        set(value) {
            field = value
            notifyChange()
        }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        super.addOnPropertyChangedCallback(callback)
        question.addChangeListener(this::notifyChange)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        super.removeOnPropertyChangedCallback(callback)
        question.removeChangeListener(this::notifyChange)
    }
}