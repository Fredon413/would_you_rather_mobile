package ca.csf.mobile2.tp2.question

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.androidannotations.annotations.Background
import org.androidannotations.annotations.EBean
import org.androidannotations.annotations.UiThread
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.create
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import java.io.IOException
import java.net.SocketTimeoutException
import java.util.*

@EBean(scope = EBean.Scope.Singleton)
class QuestionService {

    private val service: QuestionService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(JacksonConverterFactory.create(jacksonObjectMapper()))
            .build()

        service = retrofit.create()
    }

    @Background
    fun sendQuestion(
        question: Question,
        onSuccess: (Question) -> Unit,
        onServerError: () -> Unit,
        onConnectivityError: () -> Unit
    ) {
        service.addQuestion(question).execute(
            onSuccess,
            onServerError,
            onConnectivityError
        )
    }

    @Background
    fun sendChoice1(
        questionID: UUID?,
        onSuccess: (Question) -> Unit,
        onServerError: () -> Unit,
        onConnectivityError: () -> Unit
    ) {
        service.sendChoice1(questionID).execute(
            onSuccess,
            onServerError,
            onConnectivityError
        )
    }

    @Background
    fun sendChoice2(
        questionID: UUID?,
        onSuccess: (Question) -> Unit,
        onServerError: () -> Unit,
        onConnectivityError: () -> Unit
    ) {
        service.sendChoice2(questionID).execute(
            onSuccess,
            onServerError,
            onConnectivityError
        )
    }

    @Background
    fun getRandomQuestion(
        onSuccess: (Question) -> Unit,
        onServerError: () -> Unit,
        onConnectivityError: () -> Unit
    ) {
        service.getRandomQuestion().execute(
            onSuccess,
            onServerError,
            onConnectivityError
        )
    }

    @Background
    fun getQuestionById(
        questionID: UUID?,
        onSuccess: (Question) -> Unit,
        onServerError: () -> Unit,
        onConnectivityError: () -> Unit
    ) {
        service.getQuestionById(questionID).execute(
            onSuccess,
            onServerError,
            onConnectivityError
        )
    }

    @Background
    fun flagQuestion(
        questionID: UUID?,
        onSuccess: () -> Unit,
        onServerError: () -> Unit,
        onConnectivityError: () -> Unit
    ) {
        service.flagQuestion(questionID).execute(
            { onSuccess() },
            onServerError,
            onConnectivityError
        )
    }

    @UiThread
    protected fun doInUIThread(callback: () -> Unit) {
        callback()
    }

    private fun <T> Call<T>.execute(
        onSuccess: (T) -> Unit,
        onServerError: () -> Unit,
        onConnectivityError: () -> Unit
    ) {
        try {
            val response = this.execute()
            if (response.isSuccessful) {
                val body = response.body()!!
                doInUIThread { onSuccess(body) }
            } else {
                doInUIThread { onServerError() }
            }
        } catch (e: SocketTimeoutException) {
            doInUIThread { onServerError() }
        } catch (e: IOException) {
            doInUIThread { onConnectivityError() }
        }
    }

    interface QuestionService {
        @POST("api/v1/question")
        fun addQuestion(@Body question: Question): Call<Question>

        @POST("/api/v1/question/{id}/choose1")
        fun sendChoice1(@Path("id") id: UUID?): Call<Question>

        @POST("/api/v1/question/{id}/choose2")
        fun sendChoice2(@Path("id") id: UUID?): Call<Question>

        @GET("/api/v1/question/random")
        fun getRandomQuestion(): Call<Question>

        @GET("/api/v1/question/{id}")
        fun getQuestionById(@Path("id") id: UUID?): Call<Question>

        @POST("api/v1/question/{id}/flag")
        fun flagQuestion(@Path("id") id: UUID?): Call<Unit>
    }
}

private const val URL = "http://10.200.15.63:8080"