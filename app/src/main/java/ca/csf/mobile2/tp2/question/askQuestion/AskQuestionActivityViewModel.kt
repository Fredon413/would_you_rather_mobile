package ca.csf.mobile2.tp2.question.askQuestion

import androidx.databinding.BaseObservable
import androidx.databinding.Observable
import ca.csf.mobile2.tp2.question.Question
import org.parceler.Parcel
import org.parceler.ParcelConstructor
import org.parceler.Transient

@Parcel(Parcel.Serialization.BEAN)
class AskQuestionActivityViewModel @ParcelConstructor constructor(
    question: Question
) : BaseObservable() {

    var errorText = ""
        set(value) {
            field = value
            notifyChange()
        }

    var hasErrorOccurred = false
        set(value) {
            field = value
            notifyChange()
        }

    @get:Transient
    var isLoading: Boolean = false
        set(value) {
            field = value
            notifyChange()
        }

    @get:Transient
    var isChoiceMade: Boolean = false
        set(value) {
            field = value
            notifyChange()
        }

    @get:Transient
    var choice1Percent = 0
        set(value) {
            field = value
            notifyChange()
        }

    @get:Transient
    var choice2Percent = 0
        set(value) {
            field = value
            notifyChange()
        }

    var question = question
        set(value) {
            field.removeChangeListener(this::notifyChange)
            field = value
            notifyChange()
            field.addChangeListener(this::notifyChange)
        }

    @get:Transient
    val text: String
        get() = question.text

    @get:Transient
    val choice1: String
        get() = question.choice1

    @get:Transient
    val choice2: String
        get() = question.choice2

    fun convertQuestionNumberToPercent() {
        val totalChoices = question.nbChoice1 + question.nbChoice2
        if (totalChoices == 0) {
            choice1Percent = MAX_PERCENT/2
            choice2Percent = MAX_PERCENT/2
        } else {
            val percentChoice1: Int = if (question.nbChoice1 != 0)
                (question.nbChoice1 * MAX_PERCENT) / totalChoices
            else {
                question.nbChoice1
            }

            choice1Percent = percentChoice1
            choice2Percent = MAX_PERCENT - percentChoice1
        }
    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        super.addOnPropertyChangedCallback(callback)

        question.addChangeListener(this::notifyChange)
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback) {
        super.removeOnPropertyChangedCallback(callback)

        question.removeChangeListener(this::notifyChange)
    }

}